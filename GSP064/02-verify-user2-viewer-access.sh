#!/bin/bash
#

if [[ $# -lt 1 ]];then
  echo "USAGE: $0 BUCKET_NAME"
  exit 1
fi

BUCKET_NAME=$1

gcloud config configurations activate gsp064user2
gcloud storage ls gs://$BUCKET_NAME
