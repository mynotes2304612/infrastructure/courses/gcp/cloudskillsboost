#!/bin/bash
#
# This script create 2 configurations (one for each user in GSP064)

if [[ $# -lt 1 ]];then
  echo "USAGE: $0 PROJECT_ID"
  exit 1
fi

PROJECT_ID=$1

# configuring user2 configuration
gcloud config configurations create gsp064user1
echo "[*] Authenticating user1"
gcloud auth login --no-launch-browser
gcloud config set project $PROJECT_ID

# configuring user2 configuration
gcloud config configurations create gsp064user2
echo "[*] Authenticating user2"
gcloud auth login --no-launch-browser
gcloud config set project $PROJECT_ID

# activatin user1 configuration
gcloud config configurations activate gsp064user1


