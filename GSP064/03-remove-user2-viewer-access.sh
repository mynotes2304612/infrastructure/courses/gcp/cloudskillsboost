#!/bin/bash
#
# This script remove viewer premissions of user2

if [[ $# -lt 2 ]];then
  echo "USAGE: $0 PROJECT_ID USER2_EMAIL"
  exit 1
fi

PROJECT_ID=$1
USER2_EMAIL=$2

# removing viewer permissions of user2
gcloud config configurations activate gsp064user1
gcloud projects remove-iam-policy-binding $PROJECT_ID \
  --member user:$USER2_EMAIL --role roles/viewer

