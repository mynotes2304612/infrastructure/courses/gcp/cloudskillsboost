#!/bin/bash
#
# This script create a bucket using user1

# activatin user1 configuration
gcloud config configurations activate gsp064user1

BUCKET_NAME="example$RANDOM"
gcloud storage buckets create gs://$BUCKET_NAME

# uploading a simple file
echo "hello world" > sample.txt
gcloud storage cp sample.txt gs://$BUCKET_NAME
