#!/bin/bash
#
# - "Task 3. Push the Docker image to the Artifact Registry"

# creating docker image registery
gcloud artifacts repositories create valkyrie-docker-repo \
  --repository-format=docker \
  --location=us-central1 --description="Docker repository"

gcloud auth configure-docker us-central1-docker.pkg.dev

# pushing valkyrie-dev:v0.0.1 docker image to created registry
PROJECT_ID=$(gcloud config get-value project)
docker tag valkyrie-dev:v0.0.1 us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-docker-repo/valkyrie-dev:v0.0.1

docker push us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-docker-repo/valkyrie-dev:v0.0.1
