#!/bin/bash
#
# - "Task 1. Create a Docker image and store the Dockerfile"

# scripts for local marking (CHECK YOUR PROGRESS)
source <(gsutil cat gs://cloud-training/gsp318/marking/setup_marking_v2.sh)

# download application source code
gcloud source repos clone valkyrie-app

mv $HOME/marking .
mv marking/valkyrie-app .

cat >  valkyrie-app/Dockerfile << EOF
FROM golang:1.10
WORKDIR /go/src/app
COPY source .
RUN go install -v
ENTRYPOINT ["app","-single=true","-port=8080"]
EOF


echo """NOTE:\n\t Run script marking/step1_v2.sh before cliking 'Check my progress' button"""
