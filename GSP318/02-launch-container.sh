#!/bin/bash
#
# - "Task 2. Test the created Docker image"

docker run -d -p 8080:8080 valkyrie-dev:v0.0.1

echo -e """Container entrypoint:\n\thttp://127.0.0.1:8080/"""
