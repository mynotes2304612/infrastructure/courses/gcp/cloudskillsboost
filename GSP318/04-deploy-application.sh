#!/bin/bash
#
# - "Task 4. Create and expose a deployment in Kubernetes"
# 
# Before run this script perform the following tasks:
# - Replace IMAGE_NAME with your image pushed to the created docker registry
#     IMAGE <- us-central1-docker.pkg.dev/$PROJECT_ID/valkyrie-docker-repo/valkyrie-dev:v0.0.1

BASEDIR=valkyrie-app/k8s/

# deploying application
kubectl apply -f $BASEDIR/deployment.yaml
kubectl apply -f $BASEDIR/service.yaml

sleep 2

# Getting information of deployed objects
kubectl get deployments
kubectl get pods
kubectl get services
