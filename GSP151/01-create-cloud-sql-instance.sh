#!/bin/bash
#
# - "Task 1. Create a Cloud SQL instance"

gcloud sql instances create prod-instance \
  --database-version=MYSQL_5_7 --cpu=2 --memory=4GB \
  --region=us-central1 --root-password=password123
