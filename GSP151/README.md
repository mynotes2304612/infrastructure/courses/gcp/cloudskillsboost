## Cloud SQL for MySQL: Qwik Start 

### Overview

In this lab you will learn how to create and connect to a Google Cloud SQL MySQL instance and perform basic SQL operations using the Cloud Console and the mysql client.
