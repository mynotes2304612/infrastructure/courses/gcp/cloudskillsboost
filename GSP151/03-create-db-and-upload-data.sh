#!/bin/bash
#
# - "Task 3. Create a database and upload data"

# connecting to database
gcloud sql connect prod-instance --user=root

# getting database EXTERNAL IP
DB_EXTERNAL_IP=$(gcloud sql instances describe prod-instance  --format='json' | jq -r '.ipAddresses[].ipAddress')

mysql -h $DB_EXTERNAL_IP -u root -p < db.sql
