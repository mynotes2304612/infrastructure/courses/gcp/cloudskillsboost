## Terraform Fundamentals

Laboratory code: `GSP156`


What you'll learn

In this lab, you will learn how to perform the following tasks:

    Get started with Terraform in Google Cloud.
    Install Terraform from installation binaries.
    Create a VM instance infrastructure using Terraform.

