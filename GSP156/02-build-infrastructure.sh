#!/bin/bash
#
# - "Task 2. Build infrastructure"

# getting project id
PROJECT_ID=$(gcloud config get-value project)

# authentating to allow terraform deploy the infrastructure
gcloud auth application-default login

# replacing @PROJECT_ID by the id of the project
sed "s/@PROJECT_ID/$PROJECT_ID/g" instance.tf.example > instance.tf

# deploy infrastructure
terraform init
terraform apply

# show current status of infrastructure
terraform show
