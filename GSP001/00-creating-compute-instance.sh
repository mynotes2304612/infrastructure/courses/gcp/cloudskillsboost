#!/bin/bash
#
# This script create and GCP compute instance

# --boot-disk-size 25 \
gcloud compute instances create gcelab2 \
  --machine-type e2-medium
  #--image-family debian-11 \
  #--image debian-11-bullseye-v20230509

gcloud compute instances add-tags gcelab \
  --tags webserver

# open HTTP port
gcloud compute firewall-rules create allow-http-traffic \
  --allow tcp:80 \
  --description "Allow incoming tcp traffic on port 80" \
  --direction INGRESS \
  --source-ranges 0.0.0.0/0 \
  --target-tags webserver
