#!/bin/bash
#
# This script automate the following task:
# - "Task 5. Create an HTTP load balancer"

# create an instance template

gcloud compute instance-templates create lb-backend-template \
  --region=us-west1 \
   --network=default \
   --subnet=default \
   --tags=allow-health-check \
   --machine-type=e2-medium \
   --image-family=debian-11 \
   --image-project=debian-cloud \
   --metadata=startup-script='#!/bin/bash
     apt-get update
     apt-get install apache2 -y
     a2ensite default-ssl
     a2enmod ssl
     vm_hostname="$(curl -H "Metadata-Flavor:Google" \
     http://169.254.169.254/computeMetadata/v1/instance/name)"
     echo "Page served from: $vm_hostname" | \
     tee /var/www/html/index.html
     systemctl restart apache2'


# create an instance group (based on the created template)
gcloud compute instance-groups managed create lb-backend-group \
   --template=lb-backend-template --size=2 --zone=us-west1-c

# creating firewall rule to allow lb check health of instances
gcloud compute firewall-rules create fw-allow-health-check \
  --network=default \
  --action=allow \
  --direction=ingress \
  --source-ranges=130.211.0.0/22,35.191.0.0/16 \
  --target-tags=allow-health-check \
  --rules=tcp:80


# create a public static IP for lb
gcloud compute addresses create lb-ipv4-1 \
  --ip-version=IPV4 \
  --global

LB_IPADDRESS=$(gcloud compute addresses describe lb-ipv4-1 \
                  --format="get(address)" \
                  --global)

echo "LB_IPADDRESS: $LB_IPADDRESS"


# create an health check for lb
gcloud compute health-checks create http http-basic-check \
  --port 80

# create backend service (attacking http-basic-check, to monitor backend-serivce instances)
gcloud compute backend-services create web-backend-service \
  --protocol=HTTP \
  --port-name=http \
  --health-checks=http-basic-check \
  --global

gcloud compute backend-services add-backend web-backend-service \
  --instance-group=lb-backend-group \
  --instance-group-zone=us-west1-c \
  --global

# map requests to http://LB_IPADDRESS/ to an instance of web-backend-service at route /
gcloud compute url-maps create web-map-http \
    --default-service web-backend-service

# attach web-map-http to proxy
gcloud compute target-http-proxies create http-lb-proxy \
    --url-map web-map-http

# assign public static IP to proxy (lb)
gcloud compute forwarding-rules create http-content-rule \
    --address=lb-ipv4-1\
    --global \
    --target-http-proxy=http-lb-proxy \
    --ports=80


