## Set Up Network and HTTP Load Balancers 

#### What you'll learn

* Set up a network load balancer.
* Set up an HTTP load balancer.
* Get hands-on experience learning the differences between network load balancers and HTTP load balancers.
