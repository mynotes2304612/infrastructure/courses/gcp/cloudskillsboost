#!/bin/bash
# This script automate the following task:
# "Task 3. Configure the load balancing service"

# create static IP for lb
gcloud compute addresses create network-lb-ip-1

# create an HTTP health check - make requests to / to decide health of instances in target-pool
gcloud compute http-health-checks create basic-check

# create a target pool (group of instances to handle requests comming to lb)
gcloud compute target-pools create www-pool \
    --http-health-check basic-check

# attach created instances to target pool
gcloud compute target-pools add-instances www-pool \
    --instances www1,www2,www3

# Forward rule - forward requests to netwrok-lb-ip-1 into an instance of www-pool
gcloud compute forwarding-rules create www-rule \
    --ports 80 \
    --address network-lb-ip-1 \
    --target-pool www-pool


