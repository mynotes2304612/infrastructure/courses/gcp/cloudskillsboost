#!/bin/bash
#
# This script autome the following task:
# - "Task 1. Set the default region and zone for all resources"


gcloud config set compute/zone us-west1-c
gcloud config set compute/region us-west1
