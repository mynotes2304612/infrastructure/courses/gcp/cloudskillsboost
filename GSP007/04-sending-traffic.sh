#!/bin/bash
#
# This script automate the following task:
# - "Task 4. Sending traffic to your instances"

# describin www-rule (forward packets to www-pool target group)
gcloud compute forwarding-rules describe www-rule

# getting static IP attached to www-rule
IPADDRESS=$(gcloud compute forwarding-rules describe www-rule --format="json" | jq -r .IPAddress)
echo "IPADDRESS: $IPADDRESS"

N=15

for ((i=0; i < $N; i++)); do
  echo "Request $i"
  curl -m1 http://$IPADDRESS
done
