#!/bin/bash
#
# - "Task 1. Explore the default network"

# list subnets of default network
gcloud compute networks subnets list default 

# list routes if default network
gcloud compute routers list

# list VPC firewall rules
gcloud compute firewall-rules list --filter network=default \
    --sort-by priority
