#!/bin/bash
#
# Task: Task 2. Create a VPC network and VM instances
# - Create a VM instance in us-central1

if [[ $# -lt 3 ]];then
  echo """
    USAGE: $0 INSTANCE_NAME REGION ZONE
    
    EXAMPLE:
      $0 myInstance us-central1 b
    """
   exit 1
fi

INSTANCE_NAME=$1
REGION=$2
ZONE=$REGION-$3
subnetCidr=$(gcloud compute networks subnets list | grep $REGION | awk '{ print $4 }')

gcloud compute instances create $INSTANCE_NAME \
  --network mynetwork \
  --zone $ZONE \
  --machine-type e2-micro \
  --tags instances

#gcloud compute firewall-rules create allow-default \
#    --target-tags myInstances --allow icmp,ssh


