#!/bin/bash
#
# - "Task 2. Create a VPC network and VM instances"


# create network
gcloud compute networks create mynetwork \
  --subnet-mode=auto

# create firewall rules
gcloud compute firewall-rules create mynetwork-allow-icmp --network mynetwork --allow icmp --source-ranges 0.0.0.0/0
gcloud compute firewall-rules create mynetwork-allow-ssh --network mynetwork --allow tcp:22 --source-ranges 0.0.0.0/0
#gcloud compute firewall-rules create mynetwork-allow-internal --network mynetwork --allow tcp,udp,icmp --source-ranges 0.0.0.0/0
