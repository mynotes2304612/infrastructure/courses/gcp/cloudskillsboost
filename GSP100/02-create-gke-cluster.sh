#!/bin/bash
#
# - "Task 2. Create a GKE cluster"

gcloud container clusters create \
  --machine-type=e2-medium \
  --zone=us-east4-b lab-cluster 
