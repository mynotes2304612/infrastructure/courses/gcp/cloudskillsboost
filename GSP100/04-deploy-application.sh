#!/bin/bash
#
# - "Task 4. Deploy an application to the cluster"

kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0

# exposing application port via a external IP on port 8080
kubectl expose deployment hello-server --type=LoadBalancer --port 8080

# getting services
kubectl get service
