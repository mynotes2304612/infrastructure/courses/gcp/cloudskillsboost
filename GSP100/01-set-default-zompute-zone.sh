#!/bin/bash
#
# - "Task 1. Set a default compute zone"

gcloud config set compute/region us-east4
gcloud config set compute/zone us-east4-b
