#!/bin/bash
#
# - "Task 1. Clone the source repository"

git clone https://github.com/googlecodelabs/monolith-to-microservices.git
cd monolith-to-microservices && ./setup.sh
