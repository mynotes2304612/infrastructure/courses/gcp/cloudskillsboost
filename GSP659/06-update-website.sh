#!/bin/bash
#
# - "Task 6. Update website with zero downtime"

# deploy new version of monolith
GOOGLE_CLOUD_PROJECT=$(gcloud config get project)
gcloud run deploy monolith --image us-central1-docker.pkg.dev/${GOOGLE_CLOUD_PROJECT}/monolith-demo/monolith:2.0.0 --region us-central1

# describe deployed services on CLoud Run
gcloud run services describe monolith --platform managed --region us-central1
