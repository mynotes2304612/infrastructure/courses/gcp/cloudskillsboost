#!/bin/bash
#
# - "Task 3. Deploy the container to Cloud Run"

# deploy container to Cloud Run
GOOGLE_CLOUD_PROJECT=$(gcloud config get project)
gcloud run deploy monolith --image us-central1-docker.pkg.dev/${GOOGLE_CLOUD_PROJECT}/monolith-demo/monolith:1.0.0 --region us-central1


# List Cloud Run services
gcloud run services list


