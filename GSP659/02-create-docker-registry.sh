#!/bin/bash
#
# - "Task 2. Create a Docker container with Cloud Build"

# creating docker image registery
gcloud artifacts repositories create monolith-demo \
  --repository-format=docker \
  --location=us-central1 --description="Docker repository"

gcloud auth configure-docker us-central1-docker.pkg.dev
