#!/bin/bash
#


GOOGLE_CLOUD_PROJECT=$(gcloud config get project)

# reset the concurrency value to 80 (default value)
gcloud run deploy monolith --image us-central1-docker.pkg.dev/${GOOGLE_CLOUD_PROJECT}/monolith-demo/monolith:1.0.0 --region us-central1 --concurrency 80

# List Cloud Run services
gcloud run services list
