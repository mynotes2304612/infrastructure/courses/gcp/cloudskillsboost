#!/bin/bash
#

# enable APIs
gcloud services enable artifactregistry.googleapis.com \
    cloudbuild.googleapis.com \
    run.googleapis.com

# submit builded image to Cloud Build
cd monolith-to-microservices/monolith/
GOOGLE_CLOUD_PROJECT=$(gcloud config get project)
gcloud builds submit \
  --tag us-central1-docker.pkg.dev/${GOOGLE_CLOUD_PROJECT}/monolith-demo/monolith:1.0.0
