#!/bin/bash
#
# - "Task 5. Make changes to the website"

INIT_DIR=$PWD

# update index.js source code
BASEDIR=monolith-to-microservices/react-app/src/pages/Home
mv $BASEDIR/index.js $BASEDIR/index.js.bkp
cp $BASEDIR/index.js.new $BASEDIR/index.js

# build monolith
cd $INIT_DIR/monolith-to-microservices/react-app && npm run build:monolith

# load source code to Cloud Build
cd $INIT_DIR/monolith-to-microservices/monolith
GOOGLE_CLOUD_PROJECT=$(gcloud config get project)
gcloud builds submit --tag us-central1-docker.pkg.dev/${GOOGLE_CLOUD_PROJECT}/monolith-demo/monolith:2.0.0

# List Cloud Run services
gcloud run services list
