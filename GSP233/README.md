## Deploy Kubernetes Load Balancer Service with Terraform

Laboratory code: `GSP233`


### Objectives

In this lab, you will learn how to:

    Deploy a Kubernetes cluster along with a service using Terraform

