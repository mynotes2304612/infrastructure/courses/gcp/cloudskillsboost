#!/bin/bash

BASEDIR=tf-gke-k8s-service-lb
cd $BASEDIR

terraform init

PROJECT_ID=$(gcloud config get project)
terraform apply -var="region=us-central1" \
                -var="location=us-central1-c" \
                -var="project_id=$PROJECT_ID"

# getting k8s credentials to inspect deployed resources
gcloud container clusters get-credentials tf-gke-k8s  --region=us-central1
