#!/bin/bash
#
# - "Task 3. Run Terraform"

BASEDIR=terraform-google-lb-http/examples/multi-backend-multi-mig-bucket-https-lb/
PROJECT_ID=$(gcloud config get project)

cd $BASEDIR

# initializing working directory
terraform init

# Creating an execution plan
terraform plan -out=tfplan -var "project=$PROJECT_ID"

# Applying the generated plan
terraform apply tfplan
