#!/bin/bash
#
# - "Task 3. Run Terraform"

BASEDIR=terraform-google-lb-http/examples/multi-mig-http-lb/
PROJECT_ID=$(gcloud config get project)

cd $BASEDIR

# destroy deployed infrastructure
terraform destroy -var "project=$PROJECT_ID"

