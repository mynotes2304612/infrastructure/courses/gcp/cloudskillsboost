## HTTPS Content-Based Load Balancer with Terraform

Laboratory code: `GSP206`

### Objectives

In this lab, you will:

    Learn about the load balancing modules for Terraform
    Configure Terraform in the Google Cloud environment
    Create a global HTTPS Content-Based Load Balancer

