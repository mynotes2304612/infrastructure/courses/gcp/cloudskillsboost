#!/bin/bash
#
# - "Task 4. Creating pods"

# deploying monolith pod
kubectl apply -f orchestrate-with-kubernetes/kubernetes/pods/monolith.yaml
sleep 3


# getting pods
kubectl get pods

# describing monolith pod
kubectl describe pods monolith


