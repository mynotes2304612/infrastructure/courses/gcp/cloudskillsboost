#!/bin/bash
#
# - "Task 7. Creating a service"


BASEDIR=orchestrate-with-kubernetes/kubernetes

# define secrets and configmap used for monolith pod
kubectl create secret generic tls-certs --from-file $BASEDIR/tls/
kubectl create configmap nginx-proxy-conf --from-file $BASEDIR/nginx/proxy.conf
kubectl create -f $BASEDIR/pods/secure-monolith.yaml


# creating service to expose monolith
kubectl create -f $BASEDIR/services/monolith.yaml

# the sevice above expose port 31000, so open it in the firewall
gcloud compute firewall-rules create allow-monolith-nodeport \
  --allow=tcp:31000
