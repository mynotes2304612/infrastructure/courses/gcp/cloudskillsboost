#!/bin/bash
#
# - "Task 8. Adding labels to pods"


# getting pods with labels app=monolith,secure=enabled
kubectl get pods -l "app=monolith,secure=enabled"

# adding secure=enabled label to pods of secure-monolith
kubectl label pods secure-monolith 'secure=enabled'
kubectl get pods secure-monolith --show-labels

