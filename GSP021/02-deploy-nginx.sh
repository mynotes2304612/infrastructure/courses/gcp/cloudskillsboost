#!/bin/bash
#
# - "Task 2. Quick Kubernetes Demo"

# create deployment using nginx image
kubectl create deployment nginx --image=nginx:1.10.0

# listing pods
kubectl get pods

# expose nginx pods
kubectl expose deployment nginx --port 80 --type LoadBalancer

# get services
kubectl get services
