#!/bin/bash
#
# - "Task 10. Creating deployments"

BASEDIR=orchestrate-with-kubernetes/kubernetes

# deploying auth microservice
kubectl create -f $BASEDIR/deployments/auth.yaml
kubectl create -f $BASEDIR/services/auth.yaml

# deploying hello microservice
kubectl create -f $BASEDIR/deployments/hello.yaml
kubectl create -f $BASEDIR/services/hello.yaml

# deploying frontend 
kubectl create configmap nginx-frontend-conf --from-file=$BASEDIR/nginx/frontend.conf
kubectl create -f $BASEDIR/deployments/frontend.yaml
kubectl create -f $BASEDIR/services/frontend.yaml
