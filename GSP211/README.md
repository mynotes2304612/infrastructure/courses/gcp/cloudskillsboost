## Multiple VPC Networks

### Objectives

In this lab, you will learn how to perform the following tasks:

    Create custom mode VPC networks with firewall rules
    Create VM instances using Compute Engine
    Explore the connectivity for VM instances across VPC networks
    Create a VM instance with multiple network interfaces


![network-infrastructure](./images/network-infrastructure.png)
