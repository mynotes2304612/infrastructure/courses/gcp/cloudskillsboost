#!/bin/bash
#
# - "Task 2. Create VM instances"


# Create the managementnet-us-vm instance
gcloud compute instances create managementnet-us-vm \
  --zone="us-east1-c" \
  --machine-type=e2-micro \
  --network=managementnet \
  --subnet=managementsubnet-us

# Create the privatenet-us-vm instance
gcloud compute instances create privatenet-us-vm \
  --zone="us-east1-c" \
  --machine-type=e2-micro \
  --network=privatenet \
  --subnet=privatesubnet-us

# Listing the created VM instances
gcloud compute instances list --sort-by=ZONE
