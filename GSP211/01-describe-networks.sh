#!/bin/bash
#

# Listing defined networks
gcloud compute networks list

# List subnetworks of defined networks
gcloud compute networks subnets list --sort-by=NETWORK
