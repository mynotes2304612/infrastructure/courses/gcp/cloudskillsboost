#!/bin/bash
#


# Create a firewall rule to allow SSH, ICMP and RDP traffic on managementnet network
gcloud compute firewall-rules create managementnet-allow-icmp-ssh-rdp \
  --direction=INGRESS \
  --priority=1000 \
  --network=managementnet \
  --action=ALLOW \
  --rules=icmp,tcp:22,tcp:3389 \
  --source-ranges=0.0.0.0/0


# Create a firewall rule to allow SSH, ICMP and RDP traffic on privatenet network
gcloud compute firewall-rules create privatenet-allow-icmp-ssh-rdp \
  --direction=INGRESS \
  --priority=1000 \
  --network=privatenet \
  --action=ALLOW \
  --rules=icmp,tcp:22,tcp:3389 \
  --source-ranges=0.0.0.0/0


# Listing defined firewall rules of each network
gcloud compute firewall-rules list --sort-by=NETWORK
