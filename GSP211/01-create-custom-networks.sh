#!/bin/bash
#
# - "Task 1. Create custom mode VPC networks with firewall rules"



# creating managementnet custom VPC
gcloud compute networks create managementnet --subnet-mode=custom

# creating managementsubnet-us subnet of managementnet network
gcloud compute networks subnets create managementsubnet-us \
  --network=managementnet \
  --region=us-east1 \
  --range=10.130.0.0/20


# crearting privatenet network
gcloud compute networks create privatenet --subnet-mode=custom

# creating privatenet subnets
gcloud compute networks subnets create privatesubnet-us \
  --network=privatenet \
  --region=us-east1 \
  --range=172.16.0.0/24

gcloud compute networks subnets create privatesubnet-eu \
  --network=privatenet \
  --region=europe-west1 \
  --range=172.20.0.0/20

