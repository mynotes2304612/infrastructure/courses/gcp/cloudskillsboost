#!/bin/bash
#
# - "Task 4. Create a VM instance with multiple network interfaces"

gcloud compute instances create vm-appliance \
  --zone="us-east1-c" \
  --machine-type=e2-standard-4 \
  --network-interface network=privatenet,subnet=privatesubnet-us \
  --network-interface network=managementnet,subnet=managementsubnet-us \
  --network-interface network=mynetwork
