#!/bin/bash
#
# - "Ping the external IP addresses"

# making ping to management-us-vm
management_us_vm_ip=$(gcloud compute instances describe managementnet-us-vm --zone=us-east1-c   --format='get(networkInterfaces[0].accessConfigs[0].natIP)')

ping -c 3 $management_us_vm_ip


# making ping to mynet-us-vm
mynet_us_vm_ip=$(gcloud compute instances describe mynet-us-vm --zone=us-east1-c   --format='get(networkInterfaces[0].accessConfigs[0].natIP)')

ping -c 3 $mynet_us_vm_ip



# making ping to mynet-eu-vm
mynet_eu_vm_ip=$(gcloud compute instances describe mynet-eu-vm --zone=europe-west1-b   --format='get(networkInterfaces[0].accessConfigs[0].natIP)')

ping -c 3 $mynet_eu_vm_ip


# making ping to private-us-vm
private_us_vm_ip=$(gcloud compute instances describe privatenet-us-vm --zone=us-east1-c   --format='get(networkInterfaces[0].accessConfigs[0].natIP)')

ping -c 3 $private_us_vm_ip
