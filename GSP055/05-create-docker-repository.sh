#!/bin/bash

gcloud artifacts repositories create my-repository \
  --repository-format=docker \
  --location=us-central1 --description="Docker repository"

gcloud auth configure-docker us-central1-docker.pkg.dev
