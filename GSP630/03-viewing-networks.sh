#!/bin/bash
#
# - "Task 3. Viewing networks"


# listing all networks
gcloud compute networks list

# describing created network
gcloud compute networks describe labnet
