#!/bin/bash
#
# - "Task 2. Create a subnetwork"

gcloud compute networks subnets create labnet-sub \
   --network labnet \
   --region us-central1 \
   --range 10.0.0.0/28
