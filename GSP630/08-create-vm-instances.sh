#!/bin/bash
#
# - "Task 8. Create VM instances"

# create an VM instance on private-sub
gcloud compute instances create pnet-vm \
--zone=us-central1-c \
--machine-type=n1-standard-1 \
--subnet=private-sub

# create an VM instance on labnet-sub subnet
gcloud compute instances create lnet-vm \
--zone=us-central1-c \
--machine-type=n1-standard-1 \
--subnet=labnet-sub


# listing created instances
gcloud compute instances list --sort-by=ZONE
