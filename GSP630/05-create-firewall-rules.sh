#!/bin/bash
#
# - "Task 5. Creating firewall rules"


# allow ICMP and SSH traffic to labnet network
gcloud compute firewall-rules create labnet-allow-internal \
	--network=labnet \
	--action=ALLOW \
	--rules=icmp,tcp:22 \
	--source-ranges=0.0.0.0/0
