#!/bin/bash
#
# - "Task 7. Create another network"

# create privatenet network
gcloud compute networks create privatenet --subnet-mode=custom

# create private-sub subnet
gcloud compute networks subnets create private-sub \
    --network=privatenet \
    --region=us-central1 \
    --range 10.1.0.0/28


# allow ICMP and SSH traffic
gcloud compute firewall-rules create privatenet-deny \
    --network=privatenet \
    --action=DENY \
    --rules=icmp,tcp:22 \
    --source-ranges=0.0.0.0/0


# listing all defined firewall rules
gcloud compute firewall-rules list --sort-by=NETWORK
