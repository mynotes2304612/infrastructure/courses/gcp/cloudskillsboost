#!/bin/bash
#
# - "Task 4. List subnets"

# list subnets of labnet network
gcloud compute networks subnets list --network labnet
